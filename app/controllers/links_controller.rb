class LinksController < ApplicationController
  skip_before_action :authorized, only: [:open_slug]

  def shortest
    link = Link.create({ url: params[:value], slug: Link.shortest_slug, user_id: @user.id })
    if link.valid?
      render json: link
    else
      render json: {errors: link.errors.full_messages}, status: :bad_request
    end
  rescue => e
    Rails.logger.info e.message
    render json: {msg: 'Error'}, status: 500
  end

  def get_all
    render json: Link.get_all(params, @user)
  rescue => e
    Rails.logger.info e.message
    render json: {msg: 'Error'}, status: 500
  end
  
  def open_slug
    Link.clicked(params[:slug], request) do |link|
      if link
        redirect_to link.url 
      else
        render html: 'URL not found', status: :not_found
      end
    end
  rescue => e 
    Rails.logger.info e.message
    render html: 'Error', status: 500
  end

  def update
    link = Link.find(params[:id])
    link.update!(url: params[:url], slug: params[:slug])
    render json: link
  rescue => e 
    Rails.logger.info e.message
    render html: 'URL not found', status: :not_found
  end

  def destroy
    link = Link.find(params[:id])
    link.url = params[:url]
    link.destroy!
    render json: link
  rescue => e 
    Rails.logger.info e.message
    render html: 'URL not found', status: :not_found
  end

end
