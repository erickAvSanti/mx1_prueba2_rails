class UserController < ApplicationController

  def create
    @user = User.create(user_params)
    if @user.valid?
      token = encode_token({user_id: @user.id})
      render json: {user: @user, token: token}
    else
      render json: {errors: @user.errors.full_messages}, status: :bad_request
    end
  end

  private

  def user_params
    params.permit(:email, :password, :name)
  end
end
