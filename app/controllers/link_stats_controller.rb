class LinkStatsController < ApplicationController

  def get_all
    render json: LinkStat.get_all(params, @user)
  rescue => e
    Rails.logger.info e.message
    render json: {msg: 'Error'}, status: 500
  end
end
