class AuthController < ApplicationController
  skip_before_action :authorized, only: [:signup, :signin]

  def signup
    @user = User.create(user_params)
    if @user.valid?
      token = encode_token({user_id: @user.id})
      render json: {user: @user, token: token}
    else
      render json: {errors: @user.errors.full_messages}, status: :bad_request
    end
  rescue => e
    Rails.logger.info e.message
    render json: {errors: ['parámetros incorrectos']}, status: :bad_request
  end
  def signin
    @user = User.find_by(email: params[:email])

    if @user
      if @user.authenticate(params[:password])
        token = encode_token({user_id: @user.id})
        render json: {user: @user, token: token}
      else
        render json: {errors: @user.errors.full_messages}, status: :bad_request
      end
    else
      render json: {errors: ['usuario no encontrado']}, status: :not_found
    end
  end

  private

  def user_params
    params[:auth].permit(:email, :password, :name)
  end
end
