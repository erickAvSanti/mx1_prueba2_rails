class User < ApplicationRecord
  has_secure_password

  has_many :links, foreign_key: :user_id
  has_many :link_stats, foreign_key: :user_id

  validates :name, presence: {message: "el %{attribute} no debe estar vacío"}
  
  validates :email, presence: {message: "el %{attribute} no debe estar vacío"}
  validates :email, uniqueness: {case_sensitive: false, message: "el %{attribute} %{value} ya existe"}
  validates :email, length: {maximum: 50, message: "el %{attribute} supera la longitud máxima de caracteres"}
  validates :email, format: {with: URI::MailTo::EMAIL_REGEXP, message: "el %{attribute} no tiene un formato válido"}
  
  validates :password, length: {maximum: 20, message: "el %{attribute} tiene más de 20 caracteres"}
  validates :password, length: {minimum: 8, message: "el %{attribute} tiene menos de 8 caracteres"}
    
end
