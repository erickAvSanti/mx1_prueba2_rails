class LinkStat < ApplicationRecord

  belongs_to :link, foreign_key: :link_id
  belongs_to :user, foreign_key: :user_id

  def self.get_all(params, current_user)
    link = Link.where(id: params[:id], user_id: current_user.id).first
    total_user_link_stats = LinkStat.where(user_id: current_user.id).count
    link_stats = link.link_stats
    total_link_stats = link_stats.count
    percentage =  total_user_link_stats > 0 ? ( 
                  1.0 * total_link_stats / total_user_link_stats 
                  ) : 0
    {
      records: link_stats, 
      total_user_link_stats: total_user_link_stats,
      total_link_stats: total_link_stats,
      percentage: percentage
    }
  end

end
