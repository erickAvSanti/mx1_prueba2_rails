require 'device_detector'
class Link < ApplicationRecord

  has_many :link_stats, foreign_key: :link_id
  belongs_to :user, foreign_key: :user_id

  validates :slug, uniqueness: {message: 'No se pudo generar una URL única'}
  validates :url, format: {with: URI::regexp, message: "La URL no tiene un formato válido"}
  validates :url, length: {maximum: 250, too_long: 'La URL debe tener un máximo de 250 caracteres'}
  validates :url, length: {minimum: 8, too_short: 'La URL debe tener al menos 8 caracteres'}

  PAGINATION_SIZE = 20

  def self.shortest_slug(size = 6)

    charset = %w{1 2 3 4 5 6 7 9 a b c d e f g h i j k l m n o p q r s t u v 
              w x y z A B C D E F G H I J L K M N O P Q R S T U V W X Y Z}
    (0...size).map{ charset[rand(charset.size)] }.join

  end


  def self.get_all(params, current_user = nil)
    records = page(params[:page]).per(params[:pagination_size])
    {
      total_records: records.total_count, total_pages: records.total_pages,
      page: records.current_page, pagination_size: records.limit_value, records: records
    }
=begin
    pagination_size = PAGINATION_SIZE
    page = 1

    pagination_size = params[:pagination_size].to_i if params[:pagination_size].present?
    pagination_size = PAGINATION_SIZE if pagination_size > 200
    page = params[:page].to_i if params[:page].present?
    records = Link
    records = records.order(created_at: :desc)
    records = records.where(user_id: current_user.id) if current_user.present?
    total_records = records.count
    total_pages = ( total_records.to_f / pagination_size.to_f ).floor + 
                  ( total_records % pagination_size > 0 ? 1 : 0 )
    page = page > total_pages ? total_pages : (page > 0 ? page : 1)
    offset = (page - 1) * pagination_size
    offset = 0 if offset < 0
    records = records.limit(pagination_size).offset(offset)

    {
      total_records: total_records, total_pages: total_pages,
      page: page, pagination_size: pagination_size, records: records
    }
=end
  end

  def self.clicked(slug, request)
    ActiveRecord::Base.transaction do 
      link = Link.find_by_slug slug
      if link
        link.clicked += 1
        link.save!
        browser = DeviceDetector.new(request.user_agent)
        link.link_stats.create!(
          ip_address: request.remote_ip, 
          user_agent: request.user_agent, 
          os: browser.os_name,
          user_id: link.user_id
          )
        yield link
      else 
        yield false
      end
    end
  end

end
