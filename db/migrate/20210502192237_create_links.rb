class CreateLinks < ActiveRecord::Migration[6.1]
  def change
    create_table :links do |t|
      t.string :url, limit: 250
      t.string :slug, limit: 10
      t.integer :user_id, unsigned: true, null: false
      t.integer :clicked, default: 0
      t.timestamps
    end
  end
end
