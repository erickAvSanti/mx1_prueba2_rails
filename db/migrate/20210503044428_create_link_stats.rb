class CreateLinkStats < ActiveRecord::Migration[6.1]
  def change
    create_table :link_stats do |t|
      t.string :ip_address, null: false
      t.string :user_agent, null: false
      t.string :os, null: false
      t.integer :link_id, unsigned: true, null: false
      t.integer :user_id, unsigned: true, null: false
      t.timestamps
    end
  end
end
