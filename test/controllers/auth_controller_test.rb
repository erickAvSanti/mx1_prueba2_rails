require "test_helper"

class AuthControllerTest < ActionDispatch::IntegrationTest

  user = {email: 'ecavaloss@gmail.com', password: '12345678', name: 'Erick Avalos'}

  test 'sign up and sign in with user' do
    post '/account/signup', params: {auth: user}, headers: {}
    assert_equal 200, @response.code.to_i
    post '/account/signin', params: user, headers: {}
    assert_equal 200, @response.code.to_i
  end

  test 'sign up and sign in with not user' do
    post '/account/signup', params: {}, headers: {}
    assert_equal 400, @response.code.to_i
    post '/account/signin', params: {}, headers: {}
    assert_equal 404, @response.code.to_i
  end
end
