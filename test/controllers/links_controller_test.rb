require "test_helper"

class LinksControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  include SignInHelper

  test 'get links without sign in' do 
    get '/links'
    assert_equal 'get_all', @controller.action_name
    assert_equal 'application/json', @response.media_type
    assert_equal 401, @response.code.to_i
  end

  test 'get links with sign in' do 
    signup_as(DEF_USER)
    json = JSON.parse @response.body
    get '/links', headers: {Authorization: "JWT #{json['token']}"}
    assert_equal 'get_all', @controller.action_name
    assert_equal 'application/json', @response.media_type
    assert_equal 200, @response.code.to_i
  end

  test 'get shortest link without sign in' do 
    signup_as(DEF_USER)
    json = JSON.parse @response.body
    url = 'https://api.rubyonrails.org/v6.1.3.1/classes/ActiveRecord/FixtureSet.html'
    params = {value: url}
    post '/links/shortest',params: params
    assert_equal 'shortest', @controller.action_name
    assert_equal 'application/json', @response.media_type
    assert_equal 401, @response.code.to_i
  end

  test 'get shortest link with sign in, then update, then destroy' do 
    signup_as(DEF_USER)
    json = JSON.parse @response.body
    url = 'https://api.rubyonrails.org/v6.1.3.1/classes/ActiveRecord/FixtureSet.html'
    params = {value: url}
    headers = {Authorization: "JWT #{json['token']}"}
    post '/links/shortest',params: params, headers: headers
    puts @response.body
    assert_equal 'shortest', @controller.action_name
    assert_equal 'application/json', @response.media_type
    assert_equal 200, @response.code.to_i

    link = JSON.parse(@response.body)
    params = {url: 'https://cleventy.com/que-es-git-flow-y-como-funciona/', slug: 'eeeee'}
    put "/links/#{link['id']}", params: params, headers: headers
    assert_equal 'update', @controller.action_name
    assert_equal 'application/json', @response.media_type
    assert_equal 200, @response.code.to_i
    puts @response.body

    link = JSON.parse(@response.body)
    delete "/links/#{link['id']}", headers: headers
    assert_equal 'destroy', @controller.action_name
    assert_equal 'application/json', @response.media_type
    assert_equal 200, @response.code.to_i
  end

  test 'get all links' do
    signup_as(DEF_USER)
    json = JSON.parse @response.body
    headers = {Authorization: "JWT #{json['token']}"}
    get '/links', headers: headers
    puts @response.body
  end

  test 'get all stats by link ID' do
    signup_as(DEF_USER)
    json = JSON.parse @response.body
    url = 'https://api.rubyonrails.org/v6.1.3.1/classes/ActiveRecord/FixtureSet.html'
    params = {value: url}
    headers = {Authorization: "JWT #{json['token']}"}
    post '/links/shortest',params: params, headers: headers
    puts @response.body
    assert_equal 'shortest', @controller.action_name
    assert_equal 'application/json', @response.media_type
    assert_equal 200, @response.code.to_i

    link = JSON.parse(@response.body)
    headers = {Authorization: "JWT #{json['token']}"}
    get "/links/stats/#{link['id']}", headers: headers
    assert_equal 200, @response.code.to_i
    assert_equal 'application/json', @response.media_type

    headers = {'HTTP_USER_AGENT': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36'}
    get "/s/#{link['slug']}", headers: headers
    assert_equal 302, @response.code.to_i
    assert_equal 'open_slug', @controller.action_name
    assert_equal 'text/html', @response.media_type
  end
end
