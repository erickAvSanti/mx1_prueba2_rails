require "test_helper"

class AccountTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  test 'account signin' do 
    post '/account/signin'
    assert_response :not_found
  end
  test 'account signup' do 
    post '/account/signup'
    assert_response :bad_request
  end
end
