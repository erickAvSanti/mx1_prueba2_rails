Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  scope '/account' do
    post '/signin', to: 'auth#signin'
    post '/signup', to: 'auth#signup'
  end
  scope '/links' do
    post '/shortest', to: 'links#shortest'
    get '/', to: 'links#get_all'
    put '/:id', to: 'links#update'
    delete '/:id', to: 'links#destroy'
    get '/stats/:id', to: 'link_stats#get_all'
  end

  get '/s/:slug', to: 'links#open_slug'
end
